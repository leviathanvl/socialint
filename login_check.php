<?php 
	try{
		
		if(isset($_POST['usuario']) == false || isset($_POST['senha']) == false){
			die("Informe usuário e Senha");
		}

		require 'functions/password_compat/lib/password.php';
		require __DIR__."/config.php";

		$conn = new PDO(
			"mysql:host=$host;dbname=$dbname",
			$username,
			$password,
			array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
        );
        $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if(strpos($_POST['usuario'], "@") !== false){
        	$stmt = $conn->prepare("SELECT * FROM users WHERE email = :email");
        	$stmt->bindValue(":email", $_POST['usuario']);
        	$stmt->execute();
        }else{
        	$stmt = $conn->prepare("SELECT  * FROM users WHERE login = :login");
            $stmt->bindValue(":login", $_POST['usuario']);
            $stmt->execute();
        }

        if($stmt->rowCount() == 0){
        	die("Usuário Inexistente.");
        }

        $row = $stmt->fetch();

        if(password_compat($_POST["usuario"], $row["senha"])){
        	    ini_set('session.gc_maxlifetime', 604800);
                session_start();
                $_SESSION['usuario_id'] = $row['id'];
                $_SESSION['usuario'] = $row['login'];

                if($row['status']==1){
					header('Location: admin_dashboard.php');                	
                }

                header('Location: dashboard.php');
            } else {
                die('Acesso negado');
            }
	} catch (Exception $x) {
		die('Erro: ' . $x->getMessage());
	}	
	
?>