<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SocialInt - Cadastro</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

		<!-- Top menu -->	
		<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<span class="li-social">
								<a href="#"><i class="fa fa-facebook"></i></a> 
								<a href="#"><i class="fa fa-twitter"></i></a> 
								<a href="#"><i class="fa fa-envelope"></i></a> 
								<a href="#"><i class="fa fa-skype"></i></a>
							</span>
						</li>
					</ul>
				</div>
			</div>
		</nav>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 text">
                            <h1><strong >SocialInt</strong></h1>
                            <div class="description">
                            	<p>
	                            	Você está a um passo de fazer o cadastro na plataforma!
                            	</p>
                            </div>
                        </div>
                        <div class="col-sm-5 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Cadastre-se na Plataforma!</h3>
                            		<p>Preencha o formulario abaixo para obter seu acesso!</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-pencil"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form  id="form-cadastro" role="form" action="" method="post" class="registration-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-cadastro-nome">Nome</label>
			                        	<input type="text" name="form-cadastro-nome" placeholder="First name..." class="form-first-name form-control" id="form-cadastro-nome">
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-cadastro-login">Login</label>
			                        	<input type="text" name="form-first-name" placeholder="Escreva seu login..." class="form-first-name form-control" id="form-cadastro-login">
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-cadastro-senha">Senha</label>
			                        	<input type="password" name="form-cadastro-senha" placeholder="Escreva sua senha..." class="form-first-name form-control" id="form-cadastro-login">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-cadastro-estado">Estado</label>
			                        	<select name="form-cadastro-estado" class="form-last-name form-control" id="form-cadastro-estado" name="form-cadastro-estado">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-cadastro-cidade">Cidade</label>
			                        	<select name="form-cadastro-cidade" class="form-email form-control" id="form-cadastro-cidade">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-about-yourself">Um resumo sobre você.</label>
			                        	<textarea name="form-about-yourself" placeholder="Sobre você ..." 
			                        				class="form-about-yourself form-control" id="form-about-yourself"></textarea>
			                        </div>
			                        <button type="submit" class="btn">Cadastrar!</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <?php require "page_scripts.php" ?>
        <script test="text/javascript">
        	$(function(){
        		var ajaxResultEstado;
        		var ajaxResultCidade;

        		ajaxRequest({
        			url: "ajax/ajax_cadastro_estado_busca.php",
        			data: "estado=all",
        			type: "GET",
        			success: function(response){
        				ajaxResultEstado = response.result;
        				console.log(ajaxResultEstado);
        				for(var w in ajaxResultEstado){
        					$("#form-cadastro-cidade").append('<option value="' + w + '">' + ajaxResultEstado[w].nome + '</option>');
        				}

        			},
        			error: function(msg){
        				alert(msg);
        			}
        		});

        		$("#form-cadastro").on("submit", function(e){
        			var data = $("#form-cadastro").serialize();
        			$("#form-cadastro :input").prop("disabled", false);

        			ajaxPOSTRequest({
        				url: "ajax/ajax_cadastro_usuario.php",
        				data: data,
        				success: function(response){
							$("#form-cadastro :input").prop("disabled", false);
							//mensagemSucesso("Seu cadastro foi criado com sucesso.");
							alert("Seu cadastro foi criado com sucesso.");

        				},
        				error: function(msg){
        					//mensagemErro(msg);
        					alert("Ocorreu um erro no seu cadastro.");
        				}
        			});

        		});


        			$("#form-cadastro-estado").click(function(){

        			ajaxRequest({
        				url: "ajax/ajax_cadastro_cidade_busca.php",
        				data: "estado="+$("#form-cadastro-local").val(),
        				type: "GET",
        				success: function(response){
        					$("#form-cadastro-cidade").hide();
        					ajaxResultCidade = response.result;

        					for(var x in ajaxResultCidade){
        						$("#form-cadastro-cidade").append('<option value="' + x + '">' + ajaxResultCidade[x].nome + '</option>');
        					}
        					$("#form-cadastro-cidade").show();
        				},
        				error: function(msg){
        					//mensagemErro(msg);
        				}
        			});
        		});
        	});

        </script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>