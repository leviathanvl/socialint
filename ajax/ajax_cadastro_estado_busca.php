<?php 

	require __DIR__ . "/UserException.php";
	require __DIR__."/log_error.php";
	try{
		if(isset($_GET['estado'])==false || trim($_GET["estado"])==''){
			throw new UserException('Parâmetro de busca incorreto.');	
		}else if($_GET['estado'] == 'all'){
			$bool = true;
			log_error("bool = true");
		}


		require __DIR__ . "/../config.php";
        
        $conn = new PDO(
            "mysql:host=$host;dbname=$dbname",
            $username,
            $password,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
        );
        $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        //checa o id do estabelecimento
        if($bool){
        	$stmt = $conn->prepare("SELECT * FROM estados");
        }else{
        	$stmt = $conn->prepare("SELECT * FROM estados WHERE id = :id");
        	$stmt->bindValue(':id', $_GET["estado"]);
        }
        $stmt->execute();

        $result = $stmt->fetchAll();

        echo json_encode(array(
        	"error" => false,
        	"result" => $result
        ));

    }catch(Exception $ex){
    	
    	log_error($ex->getMessage());
    	echo json_encode(array(
            "error" => true,
            "message" => "Ocorreu um erro inesperado, por favor contate o suporte: jlcarruda3@gmail.com"
        ));
        if ($conn) {
            try {
                $conn->rollback();
            }
            catch (Exception $x) {
                
            }
    	}
    }

?>